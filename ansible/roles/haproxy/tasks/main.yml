---
- name: Configure Docker (geerlingguy.docker)
  include_role:
    name: geerlingguy.docker
  when:
    - ansible_facts['distribution'] != "Amazon"
    - not offline_setup or (docker_add_repo is defined and not docker_add_repo) or (docker_repo_url is defined) # Only attempt in offline setups if user is using custom repo or providing custom repo URL

- name: Configure Docker (RHEL - Amazon Linux)
  shell: "{{ 'amazon-linux-extras' if ansible_facts['distribution_major_version'] == '2' else 'yum' }} install docker -y && systemctl enable docker && systemctl start docker"
  when: ansible_facts['distribution'] == "Amazon"

- name: Configure Node Exporter (geerlingguy.node_exporter)
  include_role:
    name: geerlingguy.node_exporter
  when: not offline_setup or node_exporter_download_url is defined

- name: Create /opt/haproxy directory
  file:
    path: /opt/haproxy
    state: directory
  tags: reconfigure

- name: Create /opt/haproxy/haproxy.cfg file
  template:
    src: templates/haproxy.cfg.j2
    dest: /opt/haproxy/haproxy.cfg
    owner: '99' # HAProxy Docker User ID
    mode: '0600'
  tags: reconfigure

- name: Start HAProxy Internal Docker
  docker_container:
    name: haproxy
    image: "{{ haproxy_docker_image }}"
    pull: true
    recreate: true
    restart_policy: always
    state: started
    ports: ['80:80', '6432:6432', '1936:1936', '5432:5432', "{{ (praefect_ssl_port + ':' + praefect_ssl_port) if praefect_ssl else '2305:2305' }}"]
    volumes:
      - /opt/haproxy:/usr/local/etc/haproxy:ro
    user: '99' # HAProxy Docker User ID
    sysctls:
      net.ipv4.ip_unprivileged_port_start: 0
    etc_hosts:
      host.docker.internal: host-gateway
  register: result
  retries: 2
  delay: 5
  until: result is success
  when: "'haproxy_internal' in group_names"
  tags:
    - reconfigure
    - restart

- name: Create HAProxy certificate directories
  file:
    path: "{{ item }}"
    state: directory
  loop:
    - /opt/haproxy/ssl
    - /opt/haproxy/trusted-certs
  tags: reconfigure

- name: Copy GitLab Rails NGINX SSL CA files if configured
  copy:
    content: "{{ gitlab_rails_nginx_ssl_ca }}"
    dest: '/opt/haproxy/trusted-certs/gitlab_rails_nginx_ca.pem'
    owner: '99' # HAProxy Docker User ID
    mode: '0600'
  no_log: true
  diff: false
  when:
    - gitlab_rails_nginx_ssl
    - gitlab_rails_nginx_ssl_ca != ''

- name: Start HAProxy External Docker
  docker_container:
    name: haproxy
    image: "{{ haproxy_docker_image }}"
    pull: true
    recreate: true
    restart_policy: always
    state: started
    ports: ['80:80', '443:443', '1936:1936', "{{ gitlab_shell_ssh_port }}:{{ gitlab_shell_ssh_port }}"]
    volumes:
      - /opt/haproxy:/usr/local/etc/haproxy:ro
    user: '99' # HAProxy Docker User ID
    sysctls:
      net.ipv4.ip_unprivileged_port_start: 0
    etc_hosts:
      host.docker.internal: host-gateway
  register: result
  retries: 2
  delay: 5
  until: result is success
  when: "'haproxy_external' in group_names"
  tags:
    - reconfigure
    - restart

- name: Wait for HAProxy Docker to be running
  shell: docker inspect -f \{\{.State.Status\}\} haproxy
  register: haproxy_docker_status
  until: haproxy_docker_status.stdout == 'running'
  retries: 10
  delay: 2
  tags:
    - reconfigure
    - restart

- name: Prune any dangling Docker Images
  docker_prune:
    images: true
    timeout: 180
  when: haproxy_docker_prune
  tags: reconfigure

- name: Configure External SSL
  block:
    - name: Configure External SSL (Let's Encrypt) if configured
      import_tasks: ssl/letsencrypt.yml
      when:
        - external_url_ssl
        - external_ssl_source == 'letsencrypt'

    - name: Configure User provided certificates
      import_tasks: ssl/user.yml
      when:
        - external_url_ssl
        - external_ssl_source == 'user'

    - name: Cleanup any previous External SSL config if disabled
      import_tasks: ssl/none.yml
      when:
        - not external_url_ssl
        - "external_ssl_source == ''"
  when: "'haproxy_external' in group_names"
  tags:
    - reconfigure
    - ssl

- name: Run Custom Tasks
  block:
    - name: Check if Custom Tasks file exists
      stat:
        path: "{{ haproxy_custom_tasks_file }}"
      register: haproxy_custom_tasks_file_path
      delegate_to: localhost
      become: false

    - name: Run Custom Tasks
      include_tasks:
        file: "{{ haproxy_custom_tasks_file }}"
        apply:
          tags: custom_tasks
      when: haproxy_custom_tasks_file_path.stat.exists
  tags: custom_tasks
