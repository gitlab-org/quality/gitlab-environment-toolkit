---
- name: Retrieve AWS Partition and Account ID if not configured
  block:
    - name: Get AWS Caller Info
      amazon.aws.aws_caller_info:
        aws_profile: "{{ aws_profile if aws_profile != '' else omit }}"
      register: aws_caller_info

    - name: Extract AWS Partition from Caller Info
      set_fact:
        aws_partition: "{{ aws_caller_info.arn | regex_search(':(aws[a-zA-Z-]*):', '\\1') | first }}"
      when: aws_partition == ''

    - name: Extract AWS Account ID from Caller Info
      set_fact:
        aws_account: "{{ aws_caller_info.account }}"
      when: aws_account == ''
  when: (aws_partition == '' or aws_account == '')
  tags: cluster-autoscaler

- name: Configure AWS IAM ARN prefix
  set_fact:
    aws_iam_arn_prefix: "arn:{{ aws_partition }}:iam::{{ aws_account }}"
  tags: cluster-autoscaler

- name: Add Metrics server (AWS EKS)
  block:
    - name: Download metrics-server manifest
      ansible.builtin.get_url:
        url: "{{ aws_eks_metrics_server_manifest_url }}"
        dest: /tmp/metrics-server.yaml
        mode: '0664'

    - name: Apply metrics-server manifest to the cluster
      kubernetes.core.k8s:
        state: present
        src: /tmp/metrics-server.yaml

    - name: Clean up metrics-server manifest
      file:
        path: /tmp/metrics-server.yaml
        state: absent
  tags: metrics-server
  when: aws_eks_metrics_server_setup

- name: Configure Cluster Autoscaler (AWS EKS)
  block:
    - name: Get Kubernetes Cluster Info
      kubernetes.core.k8s_cluster_info:
      register: cluster_info
      tags: info

    - name: Get Cluster version
      set_fact:
        cluster_version: "{{ cluster_info.version.server.kubernetes.major + '.' + (cluster_info.version.server.kubernetes.minor | regex_search('[0-9]+')) }}"

    - name: Add Autoscaler repo
      kubernetes.core.helm_repository:
        name: autoscaler
        repo_url: "https://kubernetes.github.io/autoscaler"

    - name: Setup Cluster Autoscaler (AWS)
      kubernetes.core.helm:
        name: gitlab-cluster-autoscaler
        chart_ref: autoscaler/cluster-autoscaler
        chart_version: ^9
        update_repo_cache: true
        release_namespace: "kube-system"
        values:
          fullnameOverride: "gitlab-cluster-autoscaler"
          image:
            tag: "{{ cluster_autoscaler_image_tag[cluster_version] | default('v' + cluster_version + '.0') }}"
          autoDiscovery:
            clusterName: "{{ prefix }}"
          awsRegion: "{{ aws_region }}"
          rbac:
            serviceAccount:
              annotations:
                eks.amazonaws.com/role-arn: "{{ aws_iam_arn_prefix }}:role/{{ prefix }}-eks-cluster-autoscaler-role"
          extraArgs:  # https://docs.aws.amazon.com/eks/latest/userguide/autoscaling.html
            balance-similar-node-groups: true
            skip-nodes-with-system-pods: false
            skip-nodes-with-local-storage: false
  when: cloud_native_hybrid_cluster_autoscaler_setup
  tags: cluster-autoscaler

- name: Configure Gitaly StorageClass for new installs if required (AWS EKS)
  block:
    - name: Configure Gitaly StorageClass (AWS EKS)
      kubernetes.core.k8s:
        template: 'storage_classes/gitaly_gp3.aws.yml.j2'
        state: present

    - name: Set Gitaly StorageClass (AWS EKS)
      set_fact:
        gitlab_charts_gitaly_storage_class: "gitlab-gitaly-gp3"
  when:
    - gitlab_charts_gitaly_setup
    - gitlab_charts_gitaly_storage_class == ''
